from dataclasses import dataclass
@dataclass
class Post:
    def __init__(self,title,content):
        self.title = title
        self.content = content

    def dict(self):
        return {
            "title":self.title,
            "content":self.content
        }
    def mock(self):
        return "title"
