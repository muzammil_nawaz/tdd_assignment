from unittest import TestCase
from src.post import Post
from res.read_input import ReadInput
from unittest.mock import patch


class PostTest(TestCase):

    #same as @before in java
    def setUp(self):
        posts = ReadInput().get_post_input()[0].split(",")
        self.post = Post("title","content")
        self.title = posts[0].strip()
        self.content = posts[1].strip()
        self.post_1 = Post(self.title, self.content)
    #same as @after in java
    def tearDown(self):
        self.posts=None

    #mocking
    @patch("res.read_input.ReadInput")
    def test_mock(self,mock_constructor):
        mock_b = mock_constructor.return_value
        mock_b.get_post_input.return_value = ["title"]
        self.assertEqual(self.post.mock(),"title")

    def test_create_post(self):
        self.assertEqual(self.post_1.title, self.title)
        self.assertEqual(self.post_1.content, self.content)

    def test_create_post_dict(self):
        expected = {"title":self.title, "content": self.content}
        self.assertDictEqual(expected, self.post_1.dict())