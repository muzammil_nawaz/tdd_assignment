from unittest import TestCase
from src.blog import Blog
from res.read_input import ReadInput
from unittest.mock import patch

class BlogTest(TestCase):

    #same as @before in java
    def setUp(self):
        blogs = ReadInput().get_blog_input()[0].split(",")
        self.blog_o = Blog("title", "content")
        self.blog = blogs[0].strip()
        self.author = blogs[1].strip()
        self.blog1 = Blog(self.blog, self.author)

    #same as @after in java
    def tearDown(self):
        self.blogs=None

        # mocking

    @patch("res.read_input.ReadInput")
    def test_mock(self, mock_constructor):
        mock_b = mock_constructor.return_value
        mock_b.get_blog_input.return_value = ["title"]
        print(self.blog)
        self.assertEqual(self.blog_o.mock(), "title")


    def test_blog_create(self):
        self.assertEqual(self.blog1.title,self.blog)
        self.assertEqual(self.blog1.author,self.author)
        print(self.blog)
        self.assertEqual(0,len(self.blog1.posts))

    def test_repr(self):
        self.assertEqual(self.blog1.__repr__(),f"{self.blog} by {self.author}")